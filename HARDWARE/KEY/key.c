#include "key.h"

u8 KeyS[3]={0};//4个按键
u8 Key_Action_Flag=0;

void Key_GPIO_Init(void)
{
	GPIO_InitType GPIO_InitStructure;
	
	RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_GPIOA|RCC_AHB_PERIPH_GPIOB);

	GPIO_Structure_Initialize(&GPIO_InitStructure);
	
	/* Select the GPIO pin to control */
	GPIO_InitStructure.Pin          = GPIO_PIN_2 | GPIO_PIN_15;
	/* Set pin mode to general push-pull output */
	GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_INPUT;
	GPIO_InitStructure.GPIO_Pull		= GPIO_PULL_UP;
	/* Initialize GPIO */
	GPIO_Peripheral_Initialize(GPIOA, &GPIO_InitStructure);		
	

	GPIO_InitStructure.Pin          = GPIO_PIN_7;
	/* Set pin mode to general push-pull output */
	GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_INPUT;
	GPIO_InitStructure.GPIO_Pull		= GPIO_PULL_UP;

	/* Initialize GPIO */
	GPIO_Peripheral_Initialize(GPIOB, &GPIO_InitStructure);		
}

void KeyState(void)
{
	u8 button_key;//按键状态
	u8 key_state_a;//按键下标
//	static u8 key_state_num[4]={0,0,0,0};//按键判断标志
//	static u8 key_down_count[4]={0,0,0,0};//按键按下计数
//	static u8 key_up_count[4]={0,0,0,0};//按键松开计数
	static u8 key_state_num[3]={0,0,0};//按键判断标志
	static u8 key_down_count[3]={0,0,0};//按键按下计数
	static u8 key_up_count[3]={0,0,0};//按键松开计数	
	for(key_state_a = 0; key_state_a<3;key_state_a++)
	{
		switch(key_state_a)
		{		
			case 0:	button_key = KEY1;	break;
			case 1:	button_key = KEY2;	break;
			case 2:	button_key = KEY3;	break;
			default:button_key = 1;			break;
		}
		switch(key_state_num[key_state_a])
		{
			case 0:	//初始按键响应					  
				if(button_key==0)
				{
					if(key_down_count[key_state_a]++ >= 3)	//按住30ms
					{			
						key_down_count[key_state_a] = 0;//清除计数器，便于case 1 用
						key_up_count[key_state_a]   = 0;
						key_state_num[key_state_a]  = 1;
					}
				}
				else 
				{
					if(key_down_count[key_state_a]>0)key_down_count[key_state_a]--;
				}
				break;

			case 1://长按,短按 判断
				if(button_key==0)
				{
					if(key_down_count[key_state_a]++ >= 50)
					{
						//按住500ms
						KeyS[key_state_a]=LONGCLICK;//长按有效
						key_down_count[key_state_a]=0;//清除计数器，便于case 3 用
						key_up_count[key_state_a]=0;
						key_state_num[key_state_a]=4;//跳转到短按检测
					}
				}
				else//短按
				{
					if(key_up_count[key_state_a]++>=5)//松开50ms
					{

						KeyS[key_state_a]=CLICK;//单击有效
						key_down_count[key_state_a]=0;
						key_up_count[key_state_a]=0;
						key_state_num[key_state_a]=3;
					}
				}
				break;
			case 3://短按检测
				if(KeyS[key_state_a]==0||(++key_down_count[key_state_a]>=50))
				{///50*扫描周期 按键已经被执行 或者 延时一段时间后清除按键状态,并返回.
					KeyS[key_state_a]=UNCLICK;
					
					key_state_num[key_state_a]=0;
					key_down_count[key_state_a]=0;
					key_up_count[key_state_a]=0;//则返回
				}	
				Key_Action_Flag=1;//有按键按下

			
				break;
			case 4://长按检测
				if (button_key!=0)
				{
					// 按键已经释放
					KeyS[key_state_a]=UNCLICK;
					key_state_num[key_state_a]=0;
					key_down_count[key_state_a]=0;
					key_up_count[key_state_a]=0;//则清除状态并返回
				}
				Key_Action_Flag=1;

	
				break;
				
			default:key_state_num[key_state_a] = 0;	break;
		}
	}
}
