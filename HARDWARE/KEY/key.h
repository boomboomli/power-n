#ifndef _KEY_H
#define _KEY_H	

#include "n32g430.h"


#define KEY1  GPIO_Input_Pin_Data_Get(GPIOA,GPIO_PIN_2)//读取按键0
#define KEY2  GPIO_Input_Pin_Data_Get(GPIOB,GPIO_PIN_7)//读取按键1
#define KEY3  GPIO_Input_Pin_Data_Get(GPIOA,GPIO_PIN_15)//读取按键2 
//#define KEY4  GPIO_Input_Pin_Data_Get(GPIOB,GPIO_PIN_3)//读取按键3


////////////////按键引脚//////////////////////
extern u8 KeyS[3];//4个按键
extern u8 Key_Action_Flag;

//#define KEY_SD  	 		KeyS[3]//设定 菜单退出
#define KEY_UP   			KeyS[2]//数值加
#define KEY_DOWN  		KeyS[0]//数值减
#define KEY_QR   			KeyS[1]//确认 菜单确定



#define UNCLICK 	0			//未按键
#define CLICK			1     //单击 大于100ms
#define DBLCLICK 	3			//双击
#define LONGCLICK 2     //长按 大于1s

void Key_GPIO_Init(void);
void KeyState(void);

#endif
