#include "menu.h"

//a
#include "key.h"

#include "lcd_init.h"
#include "lcd.h"

#include "adc.h"

#include "chip.h"

#include "bsp_delay.h"

#include "main.h"

#include "flash.h"

#include "tim.h"


#define MAX_MENU_NUM 5
u8 menu_num = 0;

void menu_1(void);//电压电流功率
void menu_2(void);//PDO报文
void menu_3(void);//诱骗模式
void menu_4(void);//波形
void menu_5(void);//设置
u16 UP_DOWN(u16 a);
void myctime(u8 hour,u8 min,u8 sec,char *string);

void main_menu(void)
{
	switch(menu_num)
	{
		case 0:
			menu_1();
			break;	
		case 1:
			menu_2();
			break;	
		case 2:
			menu_3();
			break;	
		case 3:
			menu_4();
			break;	
		case 4:
			menu_5();
			break;	
	}
}



#define UP_DOWN_ADD 	1//加
#define UP_DOWN_LESS 	2//减

u16 MaxModify=9999;//最大设定值
u16 MinModify=0;//最小设定值

//按键加减 最大范围0-9999
u16 UP_DOWN(u16 a)
{
	u16 b,PlaceBit = 1;
	u8 updown=0;//updown=1;表示加   updown=2;表示减
	//个 十 百 千 位

	
	if(KEY_UP == CLICK)
	{	
		KEY_UP = 0;//可屏蔽
		updown = UP_DOWN_ADD;
	}
	else if(KEY_DOWN == CLICK)
	{
		KEY_DOWN = 0;//可屏蔽
		updown = UP_DOWN_LESS;
	}
	else if(KEY_UP == LONGCLICK)
	{
		updown = UP_DOWN_ADD;
	}
	else if(KEY_DOWN == LONGCLICK)
	{
		updown = UP_DOWN_LESS;
	}
	
	
	if(updown == UP_DOWN_ADD)//加数据
	{
		updown = 0;
		b = a + PlaceBit;//加倍数
		if(b <= MaxModify)	a = b;
    else 					a = MinModify;
	}
	else if(updown == UP_DOWN_LESS)//减数据
	{
		updown = 0;
		b = a - PlaceBit;//减倍数
		if(a >= PlaceBit) 
		{
			if(b>=MinModify)a=b;
			else a = MaxModify;
		}
		else 	a=MaxModify;		  
	}
	return a;
}


u8 mode_trap = 22;
u8 pdo_num = 0;
u16 pdo_max_v = 0;
u16 pdo_max_c = 0;

u8 key_val = 22;
u8 last_key_val = 22;

u8 _hour = 0;
u8 _min = 0;
u8 _sec = 0;

u8 start_tim = 0;

// 定义ADC的1LSB与实际电压电流的比值
const float volFactor = 8.471446586200685F * VOLTAGE_FACTOR;
const float curFactor = 1.646382291543582F * CURRENT_FACTOR;
// 定义电压和电流的ADC采样值
uint32_t volRaw = 0, curRaw = 0;
// 定义电压和电流值，单位mV/mA
uint16_t voltage = 0, current = 0;
// 定义功率值，单位毫瓦
uint32_t power = 0;
//电能
float power_mwh = 0;

//电能
float power_mah = 0;

uint32_t voldpRaw = 0, voldnRaw = 0;
uint16_t voldp = 0, voldn = 0;

char strbuf[256];


void menu_1(void)
{
	
			// 采样电压和电流的ADC值，16倍过采样
			volRaw = 0;
			curRaw = 0;
			voldpRaw = 0;
			voldnRaw = 0;
			for (int i = 0; i < 16; i++)
			{
				volRaw += BSP_ADC_GetData(VOLTAGE_ADC_CHANNEL);
				curRaw += BSP_ADC_GetData(CURRENT_ADC_CHANNEL);
				voldpRaw += BSP_ADC_GetData(VOLDP_ADC_CHANNEL);
				voldnRaw += BSP_ADC_GetData(VOLDN_ADC_CHANNEL);
				SysTick_Delay_Ms(20);
			}
			volRaw >>= 4;
			curRaw >>= 4;
			voldpRaw >>= 4;
			voldnRaw >>= 4;

			// 计算实际的电压、电流和功率
			voltage = volRaw * volFactor;
			current = curRaw * curFactor;
			power = current * voltage / 1000;
			
			voldp = voldpRaw * volFactor;
			voldn = voldnRaw * volFactor;

			// 将计算结果显示到显示屏上

			sprintf(strbuf, "%u.%02uV", voltage / 1000, (voltage % 1000) / 10);
			LCD_ShowString(0,0,(u8 *)strbuf,LGRAYBLUE,BLACK,24,0);

			sprintf(strbuf, "%u.%02uA", current / 1000, (current % 1000) / 10);
			LCD_ShowString(0,26,(u8 *)strbuf,LGRAY,BLACK,24,0);

			// 功率需要判断显示小数点的位置
			if (power <= 9999)
			{
				sprintf(strbuf, "%u.%02uW", power / 1000, (power % 1000) / 10);
			}
			else
			{
				sprintf(strbuf, "%u.%uW", power / 1000, (power % 1000) / 100);
			}
			LCD_ShowString(0,52,(u8 *)strbuf,BRRED,BLACK,24,0);	

			if(start_tim)//开启时间累计
			{
				power_mah = (float)(current*1000 * ((_sec*1.0/3600)+ (_min*1.0/60)+ _hour ));
				power_mwh = (float)(power*1000 * ((_sec*1.0/3600)+ (_min*1.0/60)+ _hour ));
				
			}
			

			
//			LCD_DrawLine(64,10,64,70,YELLOW);
			LCD_DrawLine(75,10,75,70,YELLOW);
			LCD_DrawLine(76,10,76,70,YELLOW);

			LCD_ShowString(80,0,"D+:",WHITE,BLACK,12,0);
			LCD_ShowString(80,10,"D-:",WHITE,BLACK,12,0);
			sprintf(strbuf, "%u.%02uv", voldp / 1000, (voldp % 1000) / 100);
			LCD_ShowString(100,0,(u8 *)strbuf,CYAN,BLACK,12,0);	
//			LCD_ShowString(100,0,"0.00v",CYAN,BLACK,12,0);
			sprintf(strbuf, "%u.%02uv", voldn / 1000, (voldn % 1000) / 100);
			LCD_ShowString(100,10,(u8 *)strbuf,CYAN,BLACK,12,0);
//			LCD_ShowString(100,10,"0.00v",CYAN,BLACK,12,0);
			
			char buf[8];
			myctime(_hour,_min,_sec,buf);
			LCD_ShowString(80,20,(u8*)buf,GREEN,BLACK,16,0);

			u32 mah = (u32)power_mah;
			sprintf(strbuf, "%04u.%02umah", mah / 1000,  (power % 1000)/ 100);
//			LCD_ShowFloatNum1(80,35,power_mah,6,YELLOW,BLACK,16);
			LCD_ShowString(80,35,(u8*)strbuf,GREEN,BLACK,16,0);
			mah = (u32)power_mwh;
			sprintf(strbuf, "%04u.%02umWh", mah / 1000,  (power % 1000) / 100);	
//			LCD_ShowFloatNum1(80,50,power_mwh,6,YELLOW,BLACK,16);			
			LCD_ShowString(80,50,(u8*)strbuf,GREEN,BLACK,16,0);
			
			
			
			LCD_ShowIntNum(100,65,mode_trap,4,BRRED,BLACK,12);


		
	if(Key_Action_Flag)
	{	
		if(KEY_QR == CLICK)
		{
			KEY_QR = UNCLICK;
			menu_num ++;
			menu_num %= MAX_MENU_NUM;
			LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80

		}
		else if(KEY_QR == LONGCLICK)
		{
			KEY_QR = UNCLICK;
			
			start_tim = 1;
		}
		else if((KEY_UP == LONGCLICK)&&(KEY_DOWN == LONGCLICK))
		{
			KEY_DOWN = UNCLICK;
			KEY_UP = UNCLICK;
			
		}
		else
		{
			Key_Action_Flag = 0;	
		
		}			
	}
}



void menu_2(void)
{
	static u16 cnt = 0;
	static u8 start_pdo = 0;
	
	cnt++;
	if(start_pdo)
	{
		FSSK_IRQHandler();
		LCD_ShowString(0,60,"read....",GRED,BLACK,12,0);
	}
	else
	{
		LCD_ShowString(0,60,"stop    ",RED,BLACK,12,0);
	}
	
	if(cnt>1)
	{
		cnt = 0;		
		LCD_ShowString(0,0,"PDO:",CYAN,BLACK,12,1);
		
		LCD_ShowIntNum(40,0,fssk_lib.pdo_num,2,CYAN,BLACK,12); 
		LCD_ShowIntNum(40,10,fssk_lib.pdo_type,2,CYAN,BLACK,12);
		LCD_ShowIntNum(40,20,fssk_lib.req_pdo_num,2,CYAN,BLACK,12);
		LCD_ShowIntNum(40,30,key_val,2,BRRED,BLACK,12);
		
	
		LCD_ShowIntNum(84-20,0,fssk_lib.pdo_max_volt[0],5,CYAN,BLACK,12);
		LCD_ShowIntNum(84-20,10,fssk_lib.pdo_max_volt[1],5,CYAN,BLACK,12);
		LCD_ShowIntNum(84-20,20,fssk_lib.pdo_max_volt[2],5,CYAN,BLACK,12);
		LCD_ShowIntNum(84-20,30,fssk_lib.pdo_max_volt[3],5,CYAN,BLACK,12);
		LCD_ShowIntNum(84-20,40,fssk_lib.pdo_max_volt[4],5,CYAN,BLACK,12);
		LCD_ShowIntNum(84-20,50,fssk_lib.pdo_max_volt[5],5,CYAN,BLACK,12);
		LCD_ShowIntNum(84-20,60,fssk_lib.pdo_max_volt[6],5,CYAN,BLACK,12);
		
		LCD_ShowIntNum(132-20,0,fssk_lib.pdo_min_volt[0],4,CYAN,BLACK,12);
		LCD_ShowIntNum(132-20,10,fssk_lib.pdo_min_volt[1],4,CYAN,BLACK,12);
		LCD_ShowIntNum(132-20,20,fssk_lib.pdo_min_volt[2],4,CYAN,BLACK,12);
		LCD_ShowIntNum(132-20,30,fssk_lib.pdo_min_volt[3],4,CYAN,BLACK,12);
		LCD_ShowIntNum(132-20,40,fssk_lib.pdo_min_volt[4],4,CYAN,BLACK,12);
		LCD_ShowIntNum(132-20,50,fssk_lib.pdo_min_volt[5],4,CYAN,BLACK,12);
		LCD_ShowIntNum(132-20,60,fssk_lib.pdo_min_volt[6],4,CYAN,BLACK,12);
	}


	MaxModify=6;//最大设定值
	MinModify=0;//最小设定值
	
	if(Key_Action_Flag)
	{
		if(KEY_QR == CLICK)
		{
			KEY_QR = UNCLICK;
			menu_num ++;
			menu_num %= MAX_MENU_NUM;
			LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80
			
			start_pdo = 0;
			
			key_val = mode_trap;
		}
		else if(KEY_QR == LONGCLICK)
		{
			KEY_QR = UNCLICK;
			
			if(start_pdo)
			{
				
				start_pdo = 0;
			}
			else
			{
				start_pdo = 1;
							
			key_val = 0;
			last_key_val = 0;
			}		
		}
		else if((KEY_UP == LONGCLICK)&&(KEY_DOWN == LONGCLICK))
		{
			KEY_DOWN = UNCLICK;
			KEY_UP = UNCLICK;
			
			pdo_num = key_val;
			pdo_max_v = fssk_lib.pdo_max_volt[key_val];
			pdo_max_c = fssk_lib.pdo_max_curr[key_val];
			LCD_ShowString(0,50,"ok",GRED,BLACK,12,0);
			eepromm_user_write();
			main_app(mode_trap,pdo_num,pdo_max_v,pdo_max_c);
			FSSK_Port_Reset();
			
			LCD_ShowString(0,50,"  ",BLACK,BLACK,12,0);			
		}			
		else
		{
			Key_Action_Flag = 0;	
			
			
			key_val = UP_DOWN(key_val);
			
			if(start_pdo)
			{
				if(key_val <= 6)
				{
					Draw_Circle(84+15,(key_val-0)*10+5,3,LGRAY);
					
					if((last_key_val != key_val)&&(last_key_val != 0)&&(last_key_val != 6))
						Draw_Circle(84+15,(last_key_val-0)*10+5,3,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 0))
						Draw_Circle(84+15,0+5,3,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 6))
						Draw_Circle(84+15,60+5,3,BLACK);
					
				}
				
			}
			last_key_val = key_val;			
		}
	}
}



void menu_3(void)//诱骗模式
{
		
	static u8 one = 1;
	
	{
		one = 0;
		LCD_ShowString(0,0,"APPLE2.4A",CYAN,BLACK,12,1);
		LCD_ShowString(0,12,"SAMSUNG2A",CYAN,BLACK,12,1);
		LCD_ShowString(0,20,"BC1.2",CYAN,BLACK,12,1);	  
		LCD_ShowString(0,30,"TYPEC",CYAN,BLACK,12,1);
		LCD_ShowString(0,40,"QC2A",CYAN,BLACK,12,1);
		LCD_ShowString(0,50,"QC2B",CYAN,BLACK,12,1);
		LCD_ShowString(0,60,"QC3A",CYAN,BLACK,12,1);
		LCD_ShowString(0,70,"QC3B",CYAN,BLACK,12,1);
		
		LCD_ShowString(60,0,"AFC",CYAN,BLACK,12,1);
		LCD_ShowString(60,10,"FCP",CYAN,BLACK,12,1);
		LCD_ShowString(60,20,"SCP",CYAN,BLACK,12,1);
		LCD_ShowString(60,30,"HISCP",CYAN,BLACK,12,1);
		LCD_ShowString(60,40,"VOOC2",CYAN,BLACK,12,1);
		LCD_ShowString(60,50,"SVOOC1",CYAN,BLACK,12,1);
		LCD_ShowString(60,60,"VOOC3",CYAN,BLACK,12,1);
		LCD_ShowString(60,70,"VOOC4",CYAN,BLACK,12,1);
		
		LCD_ShowString(100,0,"VIVO5V4A",CYAN,BLACK,12,1);
		LCD_ShowString(100,10,"VIVO10V2.25A",CYAN,BLACK,12,1);
		LCD_ShowString(100,20,"VIVO11V4A",CYAN,BLACK,12,1);
		LCD_ShowString(100,30,"MTK",CYAN,BLACK,12,1);	
		LCD_ShowString(100,40,"PPS",CYAN,BLACK,12,1);	
		LCD_ShowString(100,50,"PD",CYAN,BLACK,12,1);
		LCD_ShowString(100,60,"NULL",CYAN,BLACK,12,1);
		
		LCD_ShowString(120,70,"  ",BLACK,BLACK,12,1);
//	LCD_Fill(100,40+2,100+12*5,40+10,WHITE);
//	LCD_Fill(100,50+2,100+12*5,50+10,BLUE);
//	LCD_Fill(100,60+2,100+12*5,60+10,LGRAY);
	}
		
	MaxModify=22;//最大设定值
	MinModify=0;//最小设定值
	
	if(Key_Action_Flag)
	{	
		if(KEY_QR == CLICK)
		{
			KEY_QR = UNCLICK;
			menu_num ++;
			menu_num %= MAX_MENU_NUM;
			LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80
			
			key_val = 0;
		}
		else if((KEY_UP == LONGCLICK)&&(KEY_DOWN == LONGCLICK))
		{
			KEY_DOWN = UNCLICK;
			KEY_UP = UNCLICK;
			mode_trap = key_val;
			LCD_ShowString(140,70,"ok",GRED,BLACK,12,0);
			eepromm_user_write();
			main_app(mode_trap,pdo_num,pdo_max_v,pdo_max_c);
			LCD_ShowString(140,70,"  ",BLACK,BLACK,12,0);
//			
//			key_val = mode_trap;
		}
		else
		{
			Key_Action_Flag = 0;	
			
			key_val = UP_DOWN(key_val);
			
			if(key_val != 99)
			{
				LCD_ShowIntNum(100,70,key_val,4,BRRED,BLACK,12);
				
				if(key_val <= 7)
				{
					LCD_Fill(0,(key_val-0)*10+2,0+12*5,(key_val-0)*10+10,LGRAY);
					
					if((last_key_val != key_val)&&(last_key_val != 8)&&(last_key_val != 22))
						LCD_Fill(0,(last_key_val-0)*10+2,0+12*5,(last_key_val-0)*10+10,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 8))
						LCD_Fill(60,(last_key_val-8)*10+2,60+12*3,(last_key_val-8)*10+10,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 22))
						LCD_Fill(100,(last_key_val-16)*10+2,100+12*5,(last_key_val-16)*10+10,BLACK);
				}
				else if(key_val <= 15)
				{
					LCD_Fill(60,(key_val-8)*10+2,60+12*3,(key_val-8)*10+10,LGRAY);
					
					if((last_key_val != key_val)&&(last_key_val != 7)&&(last_key_val != 16))
						LCD_Fill(60,(last_key_val-8)*10+2,60+12*3,(last_key_val-8)*10+10,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 7))
						LCD_Fill(0,(last_key_val-0)*10+2,0+12*5,(last_key_val-0)*10+10,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 16))
						LCD_Fill(100,(last_key_val-16)*10+2,100+12*5,(last_key_val-16)*10+10,BLACK);				
				}				
				else if(key_val <= 22)
				{
					LCD_Fill(100,(key_val-16)*10+2,100+12*5,(key_val-16)*10+10,LGRAY);
					
					if((last_key_val != key_val)&&(last_key_val != 0)&&(last_key_val != 15))
						LCD_Fill(100,(last_key_val-16)*10+2,100+12*5,(last_key_val-16)*10+10,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 0))
						LCD_Fill(0,(last_key_val-0)*10+2,0+12*5,(last_key_val-0)*10+10,BLACK);
					else if((last_key_val != key_val)&&(last_key_val == 15))
						LCD_Fill(60,(last_key_val-8)*10+2,60+12*3,(last_key_val-8)*10+10,BLACK);	
				}

				
				
				last_key_val = key_val;
			}
			
			
//			main_app(mode_trap);
			
		}			
	}	
}

void menu_4(void)//波形
{

	if(Key_Action_Flag)
	{	
		if(KEY_QR == CLICK)
		{
			KEY_QR = UNCLICK;
			menu_num ++;
			menu_num %= MAX_MENU_NUM;
			LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//清屏
			
			key_val = mode_trap;
		}

		else
		{
			Key_Action_Flag = 0;	
			
			LCD_ShowString(60,0,"wave:",CYAN,BLACK,12,0);		
		}			
	}	
}

void menu_5(void)//设置
{
	static u8 flag_rotating = 0;
	if(Key_Action_Flag)
	{	
		if(KEY_QR == CLICK)
		{
			KEY_QR = UNCLICK;
			menu_num ++;
			menu_num %= MAX_MENU_NUM;
			LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80
			
			key_val = 0;
		}
		else if(KEY_QR == LONGCLICK)
		{
			KEY_QR = UNCLICK;

			
			if(flag_rotating)
			{
				flag_rotating = 0;
				LCD_WR_REG(0x36);
				LCD_WR_DATA8(0xA8); 
				LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80
			}
			else
			{
				flag_rotating = 1;
				LCD_WR_REG(0x36);
				LCD_WR_DATA8(0x78);
				LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80
			}
			
		}
		else
		{
			Key_Action_Flag = 0;	

			LCD_ShowString(30,33,"Screen Rotation",WHITE,BLACK,12,0);			
		}			
	}
	
}



//时间格式转成 字符串 
void myctime(u8 hour,u8 min,u8 sec,char *string)
{
	char  hour1,hour2, min1,min2, sec1,sec2;
	
	//时

	hour1 = hour/10 + '0';
	hour2 = hour%10 + '0';		

	//分
	min1 = min/10 + '0';
	min2 = min%10 + '0';		
	
	//秒
	sec1 = sec/10 + '0';
	sec2 = sec%10 + '0';		

	//
	sprintf(string, "%c%c:%c%c:%c%c", 
																	hour1,hour2,
																	min1,min2,
																	sec1,sec2);
}

