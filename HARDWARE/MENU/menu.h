#ifndef _MENU_H
#define _MENU_H	

#include "n32g430.h"

void main_menu(void);

extern u8 mode_trap;
extern u8 pdo_num;
extern u16 pdo_max_v;
extern u16 pdo_max_c;

extern u8 _hour ;
extern u8 _min ;
extern u8 _sec ;

extern u8 start_tim;

#endif

