#ifndef _ADC_H
#define _TIM_H	

#include "n32g430.h"

#define VOLTAGE_FACTOR       1.0F                 // 实际电压与表显电压的比值
#define CURRENT_FACTOR       1.0F                 // 实际电流与表显电流的比值

#define VOLTAGE_ADC_CHANNEL  ADC_Channel_05_PA4   // 电压采样通道编号
#define CURRENT_ADC_CHANNEL  ADC_Channel_04_PA3   // 电流采样通道编号

#define VOLDP_ADC_CHANNEL  ADC_Channel_01_PA0   // 电压采样通道编号
#define VOLDN_ADC_CHANNEL  ADC_Channel_02_PA1   // 电流采样通道编号

void BSP_ADC_Init(void);
uint16_t BSP_ADC_GetData(uint8_t ADC_Channel);
	
#endif
