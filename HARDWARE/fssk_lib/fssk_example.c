
#include "chip.h"
#include "bsp_delay.h"

extern FSSK_LIB_t fssk_lib;

void delay(unsigned int num){
//		unsigned int i;
//		for(i=0;i<num;i++){
//		}
	SysTick_Delay_Us(num);
}

//void SysTick_Delay_Ms(unsigned int num){
//		unsigned int i;
//		for(i=0;i<num;i++){
//				delay(2000);
//		}
//}


void I2C_Start(){
		PinOutInitSDA();
		FSSK_SCL_Set();
	  delay(2);
		FSSK_SDA_Set();
	  delay(2);
		FSSK_SDA_Clr();
	  delay(2);
		FSSK_SCL_Clr();
}

void I2C_ReStart(){
	PinOutInitSDA();
		FSSK_SCL_Set();
	  delay(1);
	  FSSK_SCL_Clr();
	  delay(1);
		FSSK_SDA_Set();
	  delay(1);
	  FSSK_SCL_Set();
	  delay(1);
		FSSK_SDA_Clr();
}

void I2C_Stop(){
	PinOutInitSDA();
	  FSSK_SDA_Clr();
	  delay(1);
		FSSK_SCL_Set() ;
	  delay(1);
		FSSK_SDA_Set();
}

void I2C_Wait_Ack(){
	PinInInitSDA();
		FSSK_SCL_Set();
	  delay(2);
		FSSK_SCL_Clr();
}

void Write_I2C_Byte(unsigned char I2C_Byte){
		unsigned char i;
		unsigned char m,da;
	PinOutInitSDA();
		da=I2C_Byte;
		FSSK_SCL_Clr();
	  delay(1);
		for(i=0;i<8;i++){
				m=da;
				m=m&0x80;
				if(m==0x80){
						FSSK_SDA_Set();
				}
				else {
						FSSK_SDA_Clr();
				}
				da=da<<1;
				FSSK_SCL_Set();
				delay(1);
				FSSK_SCL_Clr();
		}
		delay(1);
}

unsigned char Read_I2C_Byte(void){
		unsigned char i, da;
	PinInInitSDA();
		da=0;
	  FSSK_SCL_Clr();
	  delay(1);
		for(i=0;i<8;i++){
				if(FSSK_SDA()){
					  da |= 1<<(7-i);
				}
				FSSK_SCL_Set();
				delay(1);
				FSSK_SCL_Clr();			
		}
		return da;
}

void Write_IIC(unsigned char addr, unsigned dat){
		I2C_Start();
		Write_I2C_Byte(0x5A);
		I2C_Wait_Ack();	
		Write_I2C_Byte(addr);
		I2C_Wait_Ack();	
		Write_I2C_Byte(dat); 
		I2C_Wait_Ack();	
		I2C_Stop();
}

unsigned char Read_IIC(unsigned char addr){
	  unsigned char dat;
	  I2C_Start();
		Write_I2C_Byte(0x5A);
		I2C_Wait_Ack();	
		Write_I2C_Byte(addr);
    I2C_ReStart();	
		Write_I2C_Byte(0x5B); 
		I2C_Wait_Ack();
	  dat = Read_I2C_Byte();
	  I2C_Wait_Ack();
		I2C_Stop();
	  return dat;
}


void FSSK_GPIO_Init()
{
	GPIO_InitType GPIO_InitStructure;
	
	RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_GPIOA);

	GPIO_Structure_Initialize(&GPIO_InitStructure);
	
	/* Select the GPIO pin to control */
	GPIO_InitStructure.Pin          = GPIO_PIN_11 | GPIO_PIN_12;
	/* Set pin mode to general push-pull output */
	GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_OUT_PP;
	/* Set the pin drive current to 4MA*/
//	GPIO_InitStructure.GPIO_Current = GPIO_DS_4MA;
	/* Initialize GPIO */
	GPIO_Peripheral_Initialize(GPIOA, &GPIO_InitStructure);	
	
	FSSK_SCL_Set();

	FSSK_SDA_Set();	
}

void PinOutInitSDA(void)
{
	GPIO_InitType GPIO_InitStructure;
	
	RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_GPIOA);

	GPIO_Structure_Initialize(&GPIO_InitStructure);
	
	GPIO_InitStructure.Pin          = GPIO_PIN_11;

	GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_OUT_PP;

	GPIO_Peripheral_Initialize(GPIOA, &GPIO_InitStructure);		
}

void PinInInitSDA(void)
{
	GPIO_InitType GPIO_InitStructure;
	
	RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_GPIOA);

	GPIO_Structure_Initialize(&GPIO_InitStructure);
	
	GPIO_InitStructure.Pin          = GPIO_PIN_11;

	GPIO_InitStructure.GPIO_Mode    = GPIO_MODE_INPUT;

	GPIO_Peripheral_Initialize(GPIOA, &GPIO_InitStructure);		
}

void main_app(u8 mode,u8 pdo_num,u16 max_v,u16 max_c){	
	
		switch(mode)
		{
			case FSSK_APPLE_2P4A:
				break;
			case FSSK_SAMSUNG_2A:
				break;
			case FSSK_BC1P2:
				break;
			case FSSK_TYPEC:
				break;
			case FSSK_QC2A:
				break;
			case FSSK_QC2B:
				break;
			case FSSK_QC3A:
				break;
			case FSSK_QC3B:
				break;
			case FSSK_AFC:
				break;
			case FSSK_FCP:
				break;
			case FSSK_SCP:
				break;
			case FSSK_HISCP:
				break;
			case FSSK_VOOC2:
				break;
			case FSSK_SVOOC1:
				break;
			case FSSK_VOOC3:
				break;
			case FSSK_VOOC4:
				break;
			case FSSK_VIVO_5V4A:
				break;
			case FSSK_VIVO_10V2P25A:
				break;
			case FSSK_VIVO_11V4A:
				break;
			case FSSK_MTK:
				break;
			case FSSK_PPS:
					SysTick_Delay_Ms(500);
					fssk_lib.req_pdo_num = 0;
					FSSK_Select_Protocol(FSSK_PD);
					FSSK_Enable_Protocol(1);
					SysTick_Delay_Ms(2000);
					fssk_lib.req_pdo_num = 5;
					FSSK_Select_Voltage_Current(FSSK_PPS, 5000, 5000, 1000);
					FSSK_Enable_Protocol(1);
					SysTick_Delay_Ms(2000);
					FSSK_Select_Voltage_Current(FSSK_PPS, 6000, 6000, 1000);
					FSSK_Enable_Voltage_Current();
				break;
			case FSSK_PD:
					SysTick_Delay_Ms(500);
					fssk_lib.req_pdo_num = 0;
					FSSK_Select_Protocol(FSSK_PD);
					FSSK_Enable_Protocol(1);
					SysTick_Delay_Ms(500);
					fssk_lib.req_pdo_num = pdo_num;
					FSSK_Select_Voltage_Current(FSSK_PD, max_v, max_v, max_c);
					FSSK_Enable_Voltage_Current();
				break;

			default:
				FSSK_System_Reset();
				break;		
		}
//	  SysTick_Delay_Ms(500);
//	  fssk_lib.req_pdo_num = 0;
//		FSSK_Select_Protocol(FSSK_PPS);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(500);
//		fssk_lib.req_pdo_num = 1;
//	  FSSK_Select_Voltage_Current(FSSK_PD, 0, 0, 1500);
//	  FSSK_Enable_Voltage_Current();
	
//		  SysTick_Delay_Ms(500);
//	  fssk_lib.req_pdo_num = 0;
//		FSSK_Select_Protocol(FSSK_PPS);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(500);
////		fssk_lib.req_pdo_num = 4; // select 3.3~12V1.5A
////		FSSK_Select_Voltage_Current(FSSK_PPS,11000, 11000, 1500); //apply 11V1.5A
////		FSSK_Enable_Voltage_Current();
//		fssk_lib.req_pdo_num = 2; // select 3.3~12V1.5A
//		FSSK_Select_Voltage_Current(FSSK_PPS,12000, 12000, 1500); //apply 11V1.5A
//		FSSK_Enable_Voltage_Current();


//		  SysTick_Delay_Ms(500);
//	  fssk_lib.req_pdo_num = 0;
//		FSSK_Select_Protocol(FSSK_PPS);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(500);
//		fssk_lib.req_pdo_num = 1; // 
////		fssk_lib.pdo_type = FSSK_PDO_PPS; // 
//		FSSK_Select_Voltage_Current(FSSK_PPS,5500, 5500, 1500); //apply 11V1.5A
//		FSSK_Enable_Voltage_Current();
//	  
		/**/
//	SysTick_Delay_Ms(500);
//		FSSK_Select_Protocol(FSSK_QC2A);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(2000);
//		FSSK_Select_Voltage_Current(FSSK_QC2A, 9000, 9000, 1000);
//		FSSK_Enable_Voltage_Current();

}
