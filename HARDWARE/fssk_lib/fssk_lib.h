#ifndef _FSSK_LIB_H_
#define _FSSK_LIB_H_

#include "n32g430.h"

//Protocol type
#define FSSK_APPLE_2P4A			0
#define FSSK_SAMSUNG_2A			1
#define FSSK_BC1P2					2
#define FSSK_TYPEC					3
#define FSSK_QC2A						4
#define FSSK_QC2B						5
#define FSSK_QC3A						6
#define FSSK_QC3B						7
#define FSSK_AFC						8
#define FSSK_FCP						9
#define FSSK_SCP						10
#define FSSK_HISCP					11
#define FSSK_VOOC2					12
#define FSSK_SVOOC1					13
#define FSSK_VOOC3					14
#define FSSK_VOOC4					15
#define FSSK_VIVO_5V4A			16
#define FSSK_VIVO_10V2P25A	17
#define FSSK_VIVO_11V4A			18
#define FSSK_MTK						19
#define FSSK_PPS						20
#define FSSK_PD							21

//PDO type
#define FSSK_PDO_FIX				0
#define FSSK_PDO_PPS				1

#define FSSK_MAX_5V					1
#define FSSK_MAX_9V					3
#define FSSK_MAX_12V				7
#define FSSK_MAX_20V				15

//Sweep mode
#define FSSK_SWEEP_SAW	    0
#define FSSK_SWEEP_TRI	    1
#define FSSK_SWEEP_STEP     2

//FSSK Struct
typedef struct{
	  //SCAN Result
	  u32 prot_exists;
	  u8  qc_max_volt;
	  u8  afc_max_volt;
	  u8  fcp_max_volt;
	  u16 scp_max_volt;
	  u16 scp_min_volt;
	  u16 scp_max_curr;
	  u16 scp_min_curr;
	  u8  vivo_max_power;
	  //PD PDO
	  u8  pdo_num;
		u8  pdo_type;
	  u8  req_pdo_num;
		u16 pdo_max_volt[7];
		u16 pdo_min_volt[7];
		u16 pdo_max_curr[7];
	  u8  data_c0[32];
}FSSK_LIB_t;

void FSSK_Debug();
void FSSK_System_Reset();
void FSSK_Port_Reset();
void FSSK_Scan_Start();
void FSSK_Enable_Monitor();
void FSSK_Select_Protocol(u8 protocol);
void FSSK_Enable_Protocol(u8 ena);
void FSSK_Select_Voltage_Current(u8 protocol, u16 start_volt, u16 end_volt, u16 max_curr);
void FSSK_Enable_Voltage_Current();
void FSSK_Set_Sweep_Mode(u8 mode);
void FSSK_IRQHandler();
u16 FSSK_SCP_Calc(u8 val);

extern FSSK_LIB_t fssk_lib;

#endif
