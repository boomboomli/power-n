#ifndef _FSSK_EXAMPLE_H_
#define _FSSK_EXAMPLE_H_

#include "n32g430.h"

#define FSSK_SCL_Clr() 						GPIO_Pins_Reset(GPIOA,GPIO_PIN_12)
#define FSSK_SCL_Set() 						GPIO_Pins_Set(GPIOA,GPIO_PIN_12)
#define FSSK_SDA_Clr() 						GPIO_Pins_Reset(GPIOA,GPIO_PIN_11)
#define FSSK_SDA_Set() 						GPIO_Pins_Set(GPIOA,GPIO_PIN_11)

#define FSSK_SDA()		GPIO_Input_Pin_Data_Get(GPIOA,GPIO_PIN_11)

void delay(unsigned int num);
//void delay_ms(unsigned int num);
void I2C2_Start();
void I2C2_ReStart();
void I2C2_Stop();
void I2C2_Wait_Ack();
void Write_I2C_Byte(unsigned char I2C_Byte);
unsigned char Read_I2C_Byte(void);
void Write_IIC(unsigned char addr, unsigned dat);
unsigned char Read_IIC(unsigned char addr);

void PinOutInitSDA(void);
void PinInInitSDA(void);
void main_app(u8 mode,u8 pdo_num,u16 max_v,u16 max_c);	



#endif
