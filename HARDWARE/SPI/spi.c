#include "spi.h"

void SPI1_Init(void)
{
	GPIO_InitType GPIO_InitStructure;
	SPI_InitType SPI_InitStructure;
	
//	 RCC_Pclk2_Config(RCC_HCLK_DIV2);
	
	RCC_APB2_Peripheral_Clock_Enable(RCC_APB2_PERIPH_SPI1 );//使能SPI1
  RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_GPIOA);//使能GPIOA
	
	SPI_I2S_Reset(SPI1);

	GPIO_InitStructure.Pin =GPIO_PIN_5|GPIO_PIN_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_MODE_AF_PP;	 //复用推挽输出
	GPIO_InitStructure.GPIO_Slew_Rate = GPIO_SLEW_RATE_FAST;
  GPIO_InitStructure.GPIO_Alternate = GPIO_AF1_SPI1;
	GPIO_Peripheral_Initialize(GPIOA, &GPIO_InitStructure); //初始化GPIOA
	
	GPIO_Pins_Set(GPIOA,GPIO_PIN_5|GPIO_PIN_7);

	SPI_Initializes_Structure(&SPI_InitStructure);
	SPI_InitStructure.DataDirection = SPI_DIR_SINGLELINE_TX;//只发送模式
	SPI_InitStructure.SpiMode = SPI_MODE_MASTER;//设置SPI工作模式：主机模式
	SPI_InitStructure.DataLen = SPI_DATA_SIZE_8BITS;//设置SPI数据大小：8位帧结构
	SPI_InitStructure.CLKPOL = SPI_CLKPOL_HIGH;//串行同步时钟空闲时SCLK位高电平
	SPI_InitStructure.CLKPHA = SPI_CLKPHA_SECOND_EDGE;//串行同步时钟空第二个时钟沿捕获
	SPI_InitStructure.NSS = SPI_NSS_SOFT;//NSS信号由硬件管理
	
	SPI_InitStructure.BaudRatePres = SPI_BR_PRESCALER_4;//波特率预分频值：波特率预分频值为4
	SPI_InitStructure.FirstBit = SPI_FB_MSB;//数据传输高位先行
	SPI_InitStructure.CRCPoly = 7;//CRC值计算的多项式
	
	SPI_Initializes(SPI1, &SPI_InitStructure);//初始化SPI
	
//	SPI_CRC_Enable(SPI1);

	SPI_ON(SPI1);//使能SPI
}
//



DMA_InitType DMA_InitStructure;

u16 DMA1_MEM_LEN;//保存DMA每次数据传送的长度 	    
//DMA1的各通道配置
//这里的传输形式是固定的,这点要根据不同的情况来修改
//从存储器->外设模式/8位数据宽度/存储器增量模式
//DMA_CHx:DMA通道CHx
//cpar:外设地址
//cmar:存储器地址
//cndtr:数据传输量 
void MYDMA_Config(DMA_ChannelType* DMA_CHx,u32 cpar,u32 cmar,u16 cndtr)
{
	RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_DMA);//使能DMA传输
	
  DMA_Reset(DMA_CHx);   //将DMA的通道1寄存器重设为缺省值
	DMA1_MEM_LEN=cndtr;
	DMA_InitStructure.PeriphAddr = cpar;  //DMA外设ADC基地址
	DMA_InitStructure.MemAddr = cmar;  //DMA内存基地址
	DMA_InitStructure.Direction = DMA_DIR_PERIPH_DST;  //数据传输方向，从内存读取发送到外设
	DMA_InitStructure.BufSize = cndtr;  //DMA通道的DMA缓存的大小
	DMA_InitStructure.PeriphInc = DMA_PERIPH_INC_MODE_DISABLE;  //外设地址寄存器不变
	DMA_InitStructure.MemoryInc = DMA_MEM_INC_MODE_ENABLE;  //内存地址寄存器递增
	DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_WIDTH_BYTE;  //数据宽度为8位
	DMA_InitStructure.MemDataSize = DMA_MEM_DATA_WIDTH_BYTE; //数据宽度为8位
	DMA_InitStructure.CircularMode = DMA_CIRCULAR_MODE_DISABLE;  //工作在正常缓存模式
	DMA_InitStructure.Priority = DMA_CH_PRIORITY_MEDIUM; //DMA通道 x拥有中优先级 
	DMA_InitStructure.Mem2Mem = DMA_MEM2MEM_DISABLE;  //DMA通道x没有设置为内存到内存传输
	
	DMA_Initializes(DMA_CHx, &DMA_InitStructure);  //根据DMA_InitStruct中指定的参数初始化DMA的通道USART1_Tx_DMA_Channel所标识的寄存器
	DMA_Channel_Request_Remap(DMA_CH1, DMA_REMAP_SPI1_TX); 	
} 

void MYDMA_Config1(DMA_ChannelType* DMA_CHx,u32 cpar,u32 cmar,u16 cndtr)
{
 	RCC_AHB_Peripheral_Clock_Enable(RCC_AHB_PERIPH_DMA);//使能DMA传输
	
  DMA_Reset(DMA_CHx);   //将DMA的通道1寄存器重设为缺省值
	DMA1_MEM_LEN=cndtr;
	DMA_InitStructure.PeriphAddr = cpar;  //DMA外设ADC基地址
	DMA_InitStructure.MemAddr = cmar;  //DMA内存基地址
	DMA_InitStructure.Direction = DMA_DIR_PERIPH_DST;  //数据传输方向，从内存读取发送到外设
	DMA_InitStructure.BufSize = cndtr;  //DMA通道的DMA缓存的大小
	DMA_InitStructure.PeriphInc = DMA_PERIPH_INC_MODE_DISABLE;  //外设地址寄存器不变
	DMA_InitStructure.MemoryInc = DMA_MEM_INC_MODE_DISABLE;  //内存地址寄存器不变
	DMA_InitStructure.PeriphDataSize = DMA_PERIPH_DATA_WIDTH_HALFWORD;  //数据宽度为16位
	DMA_InitStructure.MemDataSize = DMA_MEM_DATA_WIDTH_HALFWORD; //数据宽度为16位
	DMA_InitStructure.CircularMode = DMA_CIRCULAR_MODE_DISABLE;  //工作在正常缓存模式
	DMA_InitStructure.Priority = DMA_CH_PRIORITY_MEDIUM; //DMA通道 x拥有中优先级 
	DMA_InitStructure.Mem2Mem = DMA_MEM2MEM_DISABLE;  //DMA通道x没有设置为内存到内存传输
	
	DMA_Initializes(DMA_CHx, &DMA_InitStructure);  //根据DMA_InitStruct中指定的参数初始化DMA的通道USART1_Tx_DMA_Channel所标识的寄存器
	DMA_Channel_Request_Remap(DMA_CH1, DMA_REMAP_SPI1_TX); 
} 

//开启一次DMA传输
void MYDMA_Enable(DMA_ChannelType*DMA_CHx)
{ 
	DMA_Channel_Enable(DMA_CHx);
	DMA_Current_Data_Transfer_Number_Set(DMA_CH1,DMA1_MEM_LEN);
// 	DMA_SetCurrDataCounter(DMA1_Channel3,DMA1_MEM_LEN);
 	DMA_Channel_Disable(DMA_CHx);
}	
