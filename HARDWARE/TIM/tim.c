#include "tim.h"
#include "menu.h"


//TIM5_PWM_Init(35555-1,2-1);		//�����������ʼ�� ,PWM Ƶ��=192000/35555/2=1Khz
//65535 0
// 128000/128/500
void Tim6_Init(uint16_t period, uint16_t prescaler)
{
	TIM_TimeBaseInitType TIM_TimeBaseStructure;
	
    RCC_APB1_Peripheral_Clock_Enable(RCC_APB1_PERIPH_TIM6);
	
    NVIC_InitType NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    
    NVIC_Initializes(&NVIC_InitStructure);	

    TIM_Base_Struct_Initialize(&TIM_TimeBaseStructure);
    TIM_TimeBaseStructure.Period    = period;
    TIM_TimeBaseStructure.Prescaler = prescaler;
    TIM_TimeBaseStructure.ClkDiv    = 0;
    TIM_TimeBaseStructure.CntMode   = TIM_CNT_MODE_UP;

    TIM_Base_Initialize(TIM6, &TIM_TimeBaseStructure);   	

    TIM_Base_Reload_Mode_Set(TIM6, TIM_PSC_RELOAD_MODE_IMMEDIATE);

    TIM_Interrupt_Enable(TIM6, TIM_INT_UPDATE);

    TIM_On(TIM6);	
}

u8 base1ms_flag = 0;
u8 flag_delay_run = 0;
u8 flag_key_run = 0;
u8 flag_menu_run = 0;


void TIM6_IRQHandler(void)
{
	static u16 cnt = 0;
    if (TIM_Interrupt_Status_Get(TIM6, TIM_INT_UPDATE) != RESET)
    {
        TIM_Interrupt_Status_Clear(TIM6, TIM_INT_UPDATE);
    
				base1ms_flag = 1;
				cnt++;
				cnt%=3000;
			
				if(cnt%1000 == 0)flag_delay_run = 1;
				if(cnt%10 == 0)flag_key_run = 1;
				if(cnt%200 == 0)flag_menu_run = 1;
			
				if(cnt%10 == 0)KeyState();
			
				if(start_tim)
				{
					if(cnt%1000 == 0)
					{
						_sec ++;
						if(_sec >= 60)
						{
							_sec = 0;
							_min ++;
							if(_min >= 60)
							{
								_min = 0;
								_hour ++;
								_hour %= 24;
							}
						}
					}
				}
    }
}


