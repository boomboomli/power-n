#ifndef _TIM_H
#define _TIM_H	

#include "n32g430.h"

void Tim6_Init(uint16_t period, uint16_t prescaler);

extern u8 base1ms_flag;
extern u8 flag_delay_run;
extern u8 flag_key_run ;
extern u8 flag_menu_run ;



#endif
