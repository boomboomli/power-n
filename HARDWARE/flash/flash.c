#include "flash.h"
#include "menu.h"

#define M8(adr)  (*((vu8  *) (adr)))
#define M16(adr) (*((vu16 *) (adr)))
#define M32(adr) (*((vu32 *) (adr)))

#define BASED_USERDATA_ADDRESS		((uint32_t)0x0800F800)// 0x0807F800	//页31

/*擦除指定页*/
static void FLASH_SIM_EraseEE(u32 pageAddress)
{
    FLASH_Unlock();
    FLASH_One_Page_Erase(pageAddress);
    FLASH_Lock();
}


void eepromm_user_write(void)//写入用户参数
{

	
	//擦除一页数据	一页1k字节
	FLASH_SIM_EraseEE(BASED_USERDATA_ADDRESS);
	FLASH_Unlock();
	FLASH_Word_Program(BASED_USERDATA_ADDRESS,mode_trap);
	FLASH_Word_Program(BASED_USERDATA_ADDRESS+4,pdo_num);
	FLASH_Word_Program(BASED_USERDATA_ADDRESS+8,pdo_max_v);
	FLASH_Word_Program(BASED_USERDATA_ADDRESS+12,pdo_max_c);
	FLASH_Lock();
}




void eepromm_user_read(void)//读用户参数
{
	u32 data; 
	data = (*(__IO uint32_t*)(BASED_USERDATA_ADDRESS));
	mode_trap = (u8)data; 
	if(mode_trap == 255)
		mode_trap = 22;
	
	data = (*(__IO uint32_t*)(BASED_USERDATA_ADDRESS+4));
	pdo_num = (u8)data; 
	if(pdo_num == 255)
		pdo_num = 22;	
	
	data = (*(__IO uint32_t*)(BASED_USERDATA_ADDRESS+8));
	pdo_max_v = (u8)data; 
	if(pdo_max_v == 255)
		pdo_max_v = 0;	
	
	data = (*(__IO uint32_t*)(BASED_USERDATA_ADDRESS+12));
	pdo_max_c = (u8)data; 
	if(pdo_max_c == 255)
		pdo_max_c = 0;	
}
