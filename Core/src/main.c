/*****************************************************************************
 * Copyright (c) 2019, Nations Technologies Inc.
 *
 * All rights reserved.
 * ****************************************************************************
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Nations' name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY NATIONS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL NATIONS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ****************************************************************************/
 
/**
*\*\file main.c
*\*\author Nations
*\*\version v1.0.0
*\*\copyright Copyright (c) 2019, Nations Technologies Inc. All rights reserved.
**/

#include "main.h"

#include "chip.h"

#include "lcd_init.h"
#include "lcd.h"
#include "pic.h"

#include "bsp_led.h"
#include "bsp_delay.h"

#include "tim.h"

#include "key.h"

#include "adc.h"

#include "menu.h"

#include "flash.h"

/**
 *\*\name   main.
 *\*\fun    main function.
 *\*\param  none.
 *\*\return none.
**/
int main(void)
{	

//	LED_Initialize(GPIOA, GPIO_PIN_11 );//pa11初始化，调试用
//	LED_Off(GPIOA, GPIO_PIN_11 );
	
	BSP_ADC_Init();//adc初始化
	Key_GPIO_Init();
	FSSK_GPIO_Init();

	Tim6_Init(128-1,500-1);//1ms

		LCD_Init();//LCD初始化
	LCD_Fill(0,0,LCD_W,LCD_H,BLACK);//160*80
	
	eepromm_user_read();

	main_app(mode_trap,pdo_num,pdo_max_v,pdo_max_c);
	

//		SysTick_Delay_Ms(500);
//		fssk_lib.req_pdo_num = 0;
//		FSSK_Select_Protocol(FSSK_PD);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(2000);
//		fssk_lib.req_pdo_num = 2;
//		FSSK_Select_Voltage_Current(FSSK_PPS, 5000, 5000, 1000);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(2000);
//		FSSK_Select_Voltage_Current(FSSK_PPS, 7000, 7000, 1000);
//		FSSK_Enable_Voltage_Current();

//	  SysTick_Delay_Ms(500);
//	  fssk_lib.req_pdo_num = 0;
//		FSSK_Select_Protocol(FSSK_PPS);
//		FSSK_Enable_Protocol(1);
//		SysTick_Delay_Ms(500);
//		fssk_lib.req_pdo_num = 1;
//	  FSSK_Select_Voltage_Current(FSSK_PD, 5000, 5000, 1500);
//	  FSSK_Enable_Voltage_Current();


	
	while(1)
	{
		if(flag_delay_run)
		{
			flag_delay_run = 0;	
//			LCD_ShowIntNum(84,40,keydata,5,CYAN,BLACK,12);
//			LCD_ShowFloatNum1(0,0,4.73,3,YELLOW,BLACK,24);//电压
		}
		if(flag_key_run)
		{
			flag_key_run = 0;
		}
		if(flag_menu_run)
		{
			flag_menu_run = 0;
			main_menu();
		}
		
	}


	
	
	
	
	while(1)
	{
		
		

//	  u8  pdo_num;
//		u8  pdo_type;
//	  u8  req_pdo_num;
//		u16 pdo_max_volt[7];
//		u16 pdo_min_volt[7];
//		u16 pdo_max_curr[7];		
		
		
		
//		LCD_ShowFloatNum1(0,0,4.73,3,YELLOW,BLACK,24);//电压
//		LCD_ShowString(12*4,0+4,"V",YELLOW,BLACK,16,0);
//		
//		LCD_ShowFloatNum1(0,26,0.00,3,GREEN,BLACK,24);//电流
//		LCD_ShowString(12*4,26+4,"A",GREEN,BLACK,16,0);
//		
//		LCD_ShowFloatNum1(0,52,0.00,3,RED,BLACK,24);//功率
//		LCD_ShowString(12*4,52+4,"W",RED,BLACK,16,0);
		
		/* Toggle LED2 */
//		LED_Toggle(GPIOA, GPIO_PIN_11);
		
		/* Delay 1s */
//	    SysTick_Delay_Ms(1000);
		
	}
}




//		LCD_ShowChinese(40,0,"中景园电子",RED,WHITE,16,0);
//		LCD_ShowString(10,20,"LCD_W:",RED,WHITE,16,0);
//		LCD_ShowIntNum(58,20,LCD_W,3,RED,WHITE,16);
//		LCD_ShowString(10,40,"LCD_H:",RED,WHITE,16,0);
//		LCD_ShowIntNum(58,40,LCD_H,3,RED,WHITE,16);
//		LCD_ShowFloatNum1(10,60,t,4,RED,WHITE,16);
//		t+=0.11;
//		LCD_ShowPicture(100,20,40,40,gImage_1);
